package sample;

import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public class Controller {
    public Button btnOne;
    public Button btnfour;
    public Button btnTwo;
    public Button btnThree;
    public Label lab;
    public Button add;
    public Button ans;
    public int a,b;


    public void doClick(ActionEvent actionEvent) {
        Button button=(Button)actionEvent.getSource();
        String number=lab.getText();
        lab.setText(number.concat(button.getText()));
    }


    public void doCom(ActionEvent actionEvent) {
        Button button=(Button)actionEvent.getSource();
        switch (button.getText()){
            case "+":
                a = Integer.parseInt(lab.getText());
                lab.setText("");
                break;
            case "=":
                b= Integer.parseInt(lab.getText());
                lab.setText(String.valueOf(a+b));
                break;
        }
    }
}
